![LogoIndigo](https://bitbucket.org/repo/7EGx7bA/images/3379423933-indigologo.png "Indigo logo")

# Objetivo
__Este proyecto tiene como objetivo__:
* Ofrecer un catalogo de productos para los clientes.
* Ingresar el trabajo artistico que se deberá imprimir.
* Registrar los clientes y productos solicitados.
* Ofrecer seguimiento sobre el estado de cada pedido.

# Motivación
Este proyecto surge a partir de la necesidad de ampliar los canales de ventas que posee la gráfica
brindando al cliente la posibilidad de solicitar productos de manera online.