create schema INDIGO_GRAFICA;
use indigo_grafica;

CREATE TABLE `indigo_grafica`.`person` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NULL,
  `last_name` VARCHAR(45) NULL,
  `birthdate` DATE NULL,
  `cuit` BIGINT NULL,
  `email` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));
