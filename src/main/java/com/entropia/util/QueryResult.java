package com.entropia.util;

import java.util.List;

/**
 * Created by Pelotita on 31/3/2018.
 */
public class QueryResult {

    private int totalRecords;
    private List<Object> list;

    public int getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(int totalRecords) {
        this.totalRecords = totalRecords;
    }

    public List<Object> getList() {
        return list;
    }

    public void setList(List<Object> list) {
        this.list = list;
    }
}
