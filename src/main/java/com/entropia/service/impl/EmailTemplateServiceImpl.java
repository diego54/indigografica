package com.entropia.service.impl;

import com.entropia.model.EmailTemplate;
import com.entropia.repository.EmailTemplateRepository;
import com.entropia.service.EmailTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmailTemplateServiceImpl implements EmailTemplateService {

    @Autowired
    EmailTemplateRepository emailTemplateRepository;


    @Override
    public EmailTemplate findByKey(String key) { return emailTemplateRepository.findByKey(key);}
}
