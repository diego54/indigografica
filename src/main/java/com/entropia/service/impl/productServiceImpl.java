package com.entropia.service.impl;

import com.entropia.model.Product;
import com.entropia.repository.ProductRepository;
import com.entropia.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class productServiceImpl implements ProductService{

    @Autowired
    ProductRepository productRepository;

    @Override
    public List<Product> findByCategoryId(Integer id) {
        return productRepository.findByCategoryId(id);
    }
}
