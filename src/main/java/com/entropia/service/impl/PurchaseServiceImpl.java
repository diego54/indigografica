package com.entropia.service.impl;

import com.entropia.model.Item;
import com.entropia.model.Person;
import com.entropia.model.Purchase;
import com.entropia.repository.PurchaseRepository;
import com.entropia.service.PersonService;
import com.entropia.service.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class PurchaseServiceImpl implements PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    PersonService personService;

    @Override
    public Purchase save(Purchase purchase) {
        Person p = personService.findByCuit(purchase.getOwner().getCuit());
        if(p != null){
            /*Logger.getLogger("COSO::").log(Level.INFO,"Se Encontro la persona con cuit " + p.getCuit() + " e Id " + p.getId());*/
            p.assign(purchase.getOwner());
            purchase.setOwner(p);
        }
        return purchaseRepository.save(purchase);
    }

}
