package com.entropia.service;

import com.entropia.model.Purchase;

public interface PurchaseService {
    Purchase save(Purchase purchase);
}
