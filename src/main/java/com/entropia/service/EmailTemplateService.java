package com.entropia.service;

import com.entropia.model.EmailTemplate;

public interface EmailTemplateService {

    EmailTemplate findByKey(String Key);

}
