package com.entropia.service;

import com.entropia.model.ProductCategory;

import java.util.List;

public interface ProductCategoryService {

    List<ProductCategory> findAll();
}
