package com.entropia.service;

import com.entropia.model.Product;

import java.util.List;

public interface ProductService {

    List<Product> findByCategoryId(Integer id);
}
