package com.entropia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@SpringBootApplication
@EntityScan(basePackages = {"com.entropia.model"})
public class IndeigoGraficaApplication {

	public static void main(String[] args) {
		SpringApplication.run(IndeigoGraficaApplication.class, args);
	}

    /*public static void main(String[] args) {
        ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
        resolver.setTemplateMode("HTML5");
        resolver.setSuffix(".html");
        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(resolver);
        final Context context = new Context(new Locale("es-ar"));
        String name = "John Doe";
        List<String> coso = new ArrayList<String>();
        coso.add(name);
        coso.add(name);
        coso.add(name);
        context.setVariable("name", name);
        context.setVariable("names", coso);

        final String html = templateEngine.process("templates/product-request-templates.html", context);
        System.out.printf(html);
    }*/
}
