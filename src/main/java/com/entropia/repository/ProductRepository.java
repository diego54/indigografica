package com.entropia.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.entropia.model.Product;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product,Long>{

    List<Product> findByCategoryId(Integer id);

}
