package com.entropia.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.entropia.model.ProductCategory;

public interface ProductCategoryRepository extends JpaRepository<ProductCategory,Long> {
}
