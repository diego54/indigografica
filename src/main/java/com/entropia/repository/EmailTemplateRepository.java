package com.entropia.repository;

import com.entropia.model.EmailTemplate;
import com.entropia.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailTemplateRepository extends JpaRepository<EmailTemplate,Long> {

    EmailTemplate findByKey(String key);
}
